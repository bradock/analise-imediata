import 'package:analise_imediata/rendimento_energetico.dart';
import 'package:flutter/material.dart';
import 'package:analise_imediata/teor_umidade.dart';
import 'package:analise_imediata/teor_volateis.dart';
import 'package:analise_imediata/rendimento_gravimetrico.dart';
import 'package:analise_imediata/rendimento_energetico.dart';
import 'package:analise_imediata/formulas.dart';
import 'package:analise_imediata/teor_cinzas.dart';
import 'package:analise_imediata/taxa_carbono.dart';
import 'package:analise_imediata/calculos_salvos.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  dynamic calculoClass = TeorUmidade();
  GlobalKey<FormState> _formUmidade = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SingleChildScrollView(
            child: Stack(
      children: <Widget>[
        Container(
          color: Colors.white, //5c574f 0xFFC8C8C8 Color(0xFFC8C8C8)
        ),
        Positioned(
            top: -140,
            left: -216,
            child: Container(
                height: MediaQuery.of(context).size.height - 200,
                width: 800,
                decoration: BoxDecoration(
                    color: Color(0xFF0b6e4f), shape: BoxShape.circle))), //Color(0xFF7764E0)
        Column(children: <Widget>[
          Padding(
              padding: EdgeInsets.only(
                  left: MediaQuery.of(context).size.width / 4, top: 40),
              child: Row(children: <Widget>[
                Container(
                    width: 10,
                    height: 10,
                    decoration: BoxDecoration(
                        shape: BoxShape.circle, color: Colors.white)),
                Text(
                  "Biomassa",
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 25,
                      color: Colors.white),
                ),
                Text(
                  "Plus",
                  style: TextStyle(
                      fontSize: 25,
                      color: Colors.white,
                      fontWeight: FontWeight.w200),
                )
              ])),
          Text("", style: TextStyle(color: Colors.white),),
          //TeorUmidade()
          calculoClass, // Classes de calculo
          Padding(
            padding: EdgeInsets.only(top: 20),
            child: SizedBox(
              height: 80,
              width: MediaQuery.of(context).size.width,
              child: ListView(
                scrollDirection: Axis.horizontal,
                children: <Widget>[
                  buttonCustomer(Color(0xFF1c2541), "Cálculos\nSalvos",
                      CalculosSalvos()),
                  buttonCustomer(Color(0xFF0b6e4f), "Teor de\nUmidade",
                      TeorUmidade()),
                  buttonCustomer(Color(0xFF0b6e4f), "Teor de\nVoláteis", //Color(0xFF7764E0)
                      AnaliseImediata()),
                  buttonCustomer(Color(0xFF0b6e4f), "Teor de\nCinzas",
                  TeorCinzas()),
                  buttonCustomer(Color(0xFF0b6e4f), "Carbono\nFixo",
                  TaxaCarbono()),
                  buttonCustomer(Color(0xFF0b6e4f), "Rendimento\nGravimétrico",
                      RendimentoGravimetrico()),
                  buttonCustomer(Color(0xFF0b6e4f), "Rendimento\nEnergético",
                      RendimentoEnergetico()),
                  buttonCustomer(Color(0xFF0b6e4f), "Fórmulas",
                    Formulas()),
                ],
              ),
            ),
          ),
        ]),
      ],
    )));
  }

  Widget buttonCustomer(Color cor, String nome, dynamic classeCalculo) {
    return Padding(
        padding: EdgeInsets.only(left: 20),
        child: GestureDetector(
          onTap: () {
            setState(() {
              calculoClass = classeCalculo;
            });
          },
          child: Container(
              width: 120,
              child: Center(
                child: Text(
                  nome,
                  style: TextStyle(
                      color: Colors.white, fontWeight: FontWeight.bold),
                  textAlign: TextAlign.center,
                ),
              ),
              decoration: BoxDecoration(
                  color: cor, borderRadius: BorderRadius.circular(20))),
        ));
  }
}
