import 'dart:io';
import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';
import "dart:async";
import 'dart:convert';

class TeorCinzas extends StatefulWidget {
  @override
  _TeorCinzas createState() => _TeorCinzas();
}

class _TeorCinzas extends State<TeorCinzas> {
  List _salvos = [];

   @override
  void initState() {
    super.initState();

    _readData().then((data) {
      //print(data);
      _salvos = json.decode(data);
      //print("sadas $_salvos");
      setState(() {
        _salvos = _salvos;
      });
    });
  }


  TextEditingController massaCadinhoController = TextEditingController();
  TextEditingController massaInicialController = TextEditingController();
  TextEditingController massaFinalController = TextEditingController();
  GlobalKey<FormState> _formUmidade = GlobalKey<FormState>();

  String resultadoTeorVolateisCinzas = "00.00";
  String resultadoCarbonoFixo = "00.00";
  bool painelVisible = false;

  void calcular() {
    double massaInicial = double.parse(massaInicialController.text);
    double massaFinal = double.parse(massaFinalController.text);
    double massaCadinho = double.parse(massaCadinhoController.text);
    double teorVolateisCinzas =
        ((massaFinal - massaCadinho) / massaInicial) * 100;
    double carbonoFixo = (100 - (teorVolateisCinzas * 2));
    print(teorVolateisCinzas);
    setState(() {
      painelVisible = true;
      resultadoTeorVolateisCinzas = teorVolateisCinzas.toStringAsPrecision(4);
      resultadoCarbonoFixo = carbonoFixo.toStringAsPrecision(4);
    });
  }

  void resetUmidade() {
    massaInicialController.text = "";
    massaFinalController.text = "";
    massaCadinhoController.text = "";
    setState(() {
      resultadoTeorVolateisCinzas = "00.00";
      resultadoCarbonoFixo = "00.00";
      painelVisible = false;
      _formUmidade = GlobalKey<FormState>();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(top: 16),
      child: Container(
          height: 415,
          width: 300,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              color: Color(0xFFFFFFFF)),
          child: Padding(
              padding: EdgeInsets.only(top: 5),
              child: SingleChildScrollView(
                child: Form(
                    key: _formUmidade,
                    child: Column(
                      children: <Widget>[
                        Text(
                          "Teor de Cinzas",
                          style: TextStyle(
                              fontSize: 30,
                              fontWeight: FontWeight.bold,
                              color: Color(0xFF606060)),
                          textAlign: TextAlign.center,
                        ),
                        GestureDetector(
                          onTap: () {
                            this.resetUmidade();
                          },
                          child: Icon(
                            Icons.rotate_left,
                            size: 20,
                          ),
                        ),
                        Padding(
                            padding: EdgeInsets.only(left: 10, right: 10),
                            child: TextFormField(
                                keyboardType: TextInputType.number,
                                cursorColor: Colors.purple,
                                controller: massaCadinhoController,
                                validator: (value) {
                                  if (value.isEmpty) {
                                    return "Digite o valor!";
                                  }
                                },
                                decoration: InputDecoration(
                                  labelText: "Massa do Cadinho",
                                  hintText: "Digite a massa cadinho",
                                ))),
                        Padding(
                            padding: EdgeInsets.only(left: 10, right: 10),
                            child: TextFormField(
                                keyboardType: TextInputType.number,
                                cursorColor: Colors.purple,
                                controller: massaInicialController,
                                validator: (value) {
                                  if (value.isEmpty) {
                                    return "Digite o valor!";
                                  }
                                },
                                decoration: InputDecoration(
                                  labelText: "Massa Inicial",
                                  hintText: "Digite a massa inicial",
                                ))),
                        Padding(
                            padding:
                                EdgeInsets.only(left: 10, bottom: 6, right: 10),
                            child: TextFormField(
                                keyboardType: TextInputType.number,
                                cursorColor: Colors.purple,
                                controller: massaFinalController,
                                validator: (value) {
                                  if (value.isEmpty) {
                                    return "Digite o valor!";
                                  }
                                },
                                decoration: InputDecoration(
                                  labelText: "Massa Final (Cadinho + Amostra)",
                                  hintText: "Digite a massa final",
                                ))),
                        Padding(
                          padding: EdgeInsets.only(left: 60, right: 60),
                          child: Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                RaisedButton(
                                    child: Text("Calcular",
                                        style: TextStyle(color: Colors.white)),
                                    color: Color(0xFF0b6e4f),
                                    onPressed: () {
                                      if (_formUmidade.currentState
                                          .validate()) {
                                        this.calcular();
                                      }
                                    }),
                                Spacer(),
                                RaisedButton(
                                    child: Text("Salvar",
                                        style: TextStyle(color: Colors.white)),
                                    color: Color(0xFF1c2541),
                                    onPressed: () {
                                      if (_formUmidade.currentState
                                              .validate() &&
                                          painelVisible == true) {
                                        this._modalSalvar();
                                      }
                                    })
                              ]),
                        ),
                        Visibility(
                          visible: painelVisible,
                          child: Column(
                            children: <Widget>[
                              Padding(
                                  padding: EdgeInsets.only(left: 15, top: 0),
                                  child: ListTile(
                                    title: Text(
                                        "Teor de Cinzas: $resultadoTeorVolateisCinzas %"),
                                    subtitle: Text("Taxa teor de cinzas"),
                                    leading: Icon(
                                      Icons.check_circle,
                                      color: Color(0xFF1c2541),
                                    ),
                                  )),
                            ],
                          ),
                        )
                      ],
                    )),
              ))),
    );
  }

  Future<File> _getFile() async {
    final directory = await getApplicationDocumentsDirectory();
    return File("${directory.path}/data.json");
  }

  Future<File> _saveData() async {
    String data = json.encode(_salvos);
    final file = await _getFile();
    return file.writeAsString(data);
  }

   Future<String> _readData() async {
    try {
      final file = await _getFile();
      return file.readAsString();
    } catch (e) {
      return null;
    }
  }

  Future<void> _modalSalvar() async {
    TextEditingController nomeSave = TextEditingController();
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Salvar dados'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text('Os dados salvos são armazenados na página \'Cálculos Salvos\'. '),
                TextField(
                  controller: nomeSave,
                  keyboardType: TextInputType.text,
                  decoration: InputDecoration(
                    labelText: "Nome para identificação do cálculo"
                  ),
                )
              ],
            ),
          ),
          actions: <Widget>[
            FlatButton(
              child: Text('Salvar'),
              onPressed: () {
                String nome = nomeSave.text;
                if(nome == ""){
                  nome = "Salvo sem nome";
                }

                 Map<String, dynamic> novoSalvo = new Map();
                 novoSalvo["title"] = nome;
                 novoSalvo["calculo"] = "Teor de Cinzas: $resultadoTeorVolateisCinzas %";
                 _salvos.add(novoSalvo);
                 _saveData();
                    
                Navigator.of(context).pop();
              },
            ),
            FlatButton(
            child: Text("Fechar"),
            onPressed: (){
              Navigator.of(context).pop();
            }
          )
          ],
        );
      },
    );
}

}
