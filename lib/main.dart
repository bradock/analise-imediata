import 'package:flutter/material.dart';
import 'package:analise_imediata/home.dart';
import 'package:splashscreen/splashscreen.dart';

void main(){
  runApp(MaterialApp(
    title: "Análise Imediata",
    home: Splash(),
  ));
}

class Splash extends StatefulWidget {
  @override
  _SplashState createState() => _SplashState();
}

class _SplashState extends State<Splash> {
  @override
  Widget build(BuildContext context) {
    return SplashScreen(
      seconds: 3, //3
      navigateAfterSeconds: new Home(),
      title: new Text('+Biomassa',
      style: new TextStyle(
        fontWeight: FontWeight.bold,
        fontSize: 30.0,
        color: Colors.white
      ),),
      loadingText: Text("@AsgardCode", style: TextStyle(color: Colors.white),),
      backgroundColor: Color(0xFF0b6e4f),
      styleTextUnderTheLoader: new TextStyle(),
      photoSize: 100.0,
      loaderColor: Colors.pink
    );
  }
}

