import 'dart:io';
import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';
import "dart:async";
import 'dart:convert';

class CalculosSalvos extends StatefulWidget {
  @override
  _CalculosSalvos createState() => _CalculosSalvos();
}

class _CalculosSalvos extends State<CalculosSalvos> {
  List _salvo = [];

  @override
  void initState() {
    super.initState();

    _readData().then((data) {
      //print(data);
      _salvo = json.decode(data);
      print("sadas $_salvo");
      setState(() {
        _salvo = _salvo;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(top: 16),
      child: Container(
          height: 415,
          width: 300,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              color: Color(0xFFFFFFFF)),
          child: Padding(
              padding: EdgeInsets.only(top: 5),
              child: SingleChildScrollView(
                  child: Column(children: <Widget>[
                  Text(
                    "* Registro salvo no diretório do aplicativo",
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 12, color: Colors.black26),
                  ),
                SizedBox(
                  height: 400,
                  width: MediaQuery.of(context).size.width,
                  child: ListView.builder(
                    itemCount: _salvo.length,
                    itemBuilder: (context, item) {
                      return
                      ListTile(
                        title: Text(_salvo[item]['title']),
                        subtitle: Text(_salvo[item]['calculo']),
                        leading: Icon(Icons.save, color: Color(0xFF1c2541),),
                      );
                    },
                  ),
                ),
              ])))),
    );
  }

  Future<File> _getFile() async {
    final directory =
        await getApplicationDocumentsDirectory(); // pega o diretorio para salvar ou ios ou androif
    return File("${directory.path}/data.json");
  }

  Future<String> _readData() async {
    try {
      final file = await _getFile();
      return file.readAsString();
    } catch (e) {
      return null;
    }
  }
}
