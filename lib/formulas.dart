import 'package:flutter/material.dart';

class Formulas extends StatefulWidget {
  @override
  _Formulas createState() => _Formulas();
}

class _Formulas extends State<Formulas> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(top: 16),
      child: Container(
          height: 415,
          width: 300,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              color: Color(0xFFFFFFFF)),
          child: Padding(
              padding: EdgeInsets.only(top: 5),
              child: SingleChildScrollView(
                child: Column(
                  children: <Widget> [
                    Padding(
                      padding: EdgeInsets.only(bottom: 10),
                      child: Text("Fórmulas", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),) ,
                    ),
                    imageLista("images/f1.png"),
                    imageLista("images/f2.png"),
                    imageLista("images/f3.png"),
                    imageLista("images/f4.png"),
                    imageLista("images/f5.png"),
                    imageLista("images/f6.png"),
                    imageLista("images/f7.png")
                  ]
                )
              ))),
    );
  }
}

Widget imageLista(String urlFoto){
  return Image.asset(
    urlFoto,
    fit: BoxFit.cover,
    height: 120,
  );
}
